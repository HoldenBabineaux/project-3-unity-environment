Envriornment is currently working as intended!

I had a bit of trouble with the shaders and rendering, as it seems some of the textures are still coming out pixelated.
I did some postprocessing on the FP Camera to add motion blurring, fog, and some bloom textures but it still seems as if
rendering is still an issue.

The FP controller is stable, but could be better. I modified the physics of the controller slightly.

Bugs:
Some terrain clipping here and there, and houses/farms have an odd clipping region around them. Tried to fix with no avail.
When using the FP controller, objects are not rendering quickly even with max game settings and the FP camera settings properly
adjusted.

Tried turning AA off within the game itself and added it with postprocessing in the FP camera. This seemed to have helped slightly,
but still some rendering issues.